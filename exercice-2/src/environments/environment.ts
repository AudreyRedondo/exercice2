
export const environment = {
    production: false,
    apiUrl: "http://localhost:4200/assets/data/",
    moviesEndpoint: "movies.json"
};
