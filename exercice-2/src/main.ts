import { bootstrapApplication, BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { importProvidersFrom } from "@angular/core";
import { AppComponent } from './app/app.component';


bootstrapApplication( AppComponent, {
    providers: [importProvidersFrom( [BrowserModule, BrowserAnimationsModule] )]
} );