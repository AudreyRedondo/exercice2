import { MovieComponent } from './components/movies/movie/movie.component';
import { Component } from '@angular/core';
import { MovieService } from './services/movie/movie.service';

@Component( {
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    standalone: true,
    imports: [MovieComponent],
    providers: [MovieService]
} )
export class AppComponent
{
}
