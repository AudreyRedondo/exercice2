import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { map, Observable } from "rxjs";
import { IMovie } from "src/app/models/i-movie.interface";
import { environment } from "src/environments/environment";

@Injectable( {
    providedIn: 'root'
} )
export class MovieService
{
    private readonly _getUrl = environment.apiUrl + environment.moviesEndpoint;
    constructor( private _http: HttpClient ) { }

    public getMovies(): Observable<ReadonlyArray<IMovie>>
    {
        return this._http
            .get<IMovie[]>( this._getUrl )
            .pipe(
                map( ( movies ) =>
                    [...movies]
                        .sort( ( n1, n2 ) => n1.id - n2.id )
                        .map( ( movie ) => movie )
                )
            );
    }
}
