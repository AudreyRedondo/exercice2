import { AsyncPipe, NgFor, NgForOf, NgIf } from '@angular/common';
import { HttpClientModule } from "@angular/common/http";
import { Component, OnInit } from '@angular/core';
import { filter, map, Observable } from 'rxjs';
import { IMovie } from 'src/app/models/i-movie.interface';
import { MovieService } from 'src/app/services/movie/movie.service';

@Component( {
    selector: 'app-movie',
    templateUrl: './movie.component.html',
    styleUrls: ['./movie.component.scss'],
    standalone: true,
    providers: [MovieService],
    imports: [HttpClientModule, NgIf, NgForOf, AsyncPipe]
} )
export class MovieComponent
{
    public readonly movies$: Observable<ReadonlyArray<IMovie>>;

    constructor( private _service: MovieService )
    {
        this.movies$ = this._getMovies();
    }

    private _getMovies(): Observable<ReadonlyArray<IMovie>>
    {
        return this._service.getMovies().pipe(
            filter( ( movie ) => movie && movie.length >= 0 ),
            map( ( movie ) => movie )
        );
    }
}
