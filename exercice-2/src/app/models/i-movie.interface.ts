export interface IMovie
{
    id: number,
    title: string,
    description: string;
    picture: string
}